#!/usr/bin/env python3
import csv
import argparse

def convert_uuid(uuid, name):
    uuid = uuid.replace('-','')
    result = "static struct bt_uuid_128 {0}_uuid = BT_UUID_INIT_128(\n".format(name)
    result += '        '
    for i,symb in enumerate(reversed(uuid)):
        if i == 16 : result += '\n        '
        if not i % 2:
            result += '0x'
            result += '0' + symb
        else:
            result = result[0:len(result)-2] + symb + result[len(result)-1:]
        if i % 2 and i < 31 : result += ','
        elif i == 31 : result += ');'
    result += '\n'
    return result

def convert(uuid, name, description):
    # print(uuid, name, description)
    print("/* {0} {1} */".format(description, uuid))
    print(convert_uuid(uuid, name))

def convert_all(file):
    with open(file) as csvfile:
        uuids_reader = csv.DictReader(csvfile, delimiter=',')
        for row in uuids_reader:
            convert(row.get('uuid'), row.get('name'), row.get('description'))

def main():
    parser = argparse.ArgumentParser(description='Process UUIDS')
    parser.add_argument('input', metavar='File',
                    help='file with UUIDs')
    args = parser.parse_args()
    convert_all(args.input)

if __name__ == "__main__":
    main()
